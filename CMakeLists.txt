cmake_minimum_required(VERSION 3.28)
project(challenge_itat)

set(CMAKE_CXX_STANDARD 17)
#create standalone executable with static libraries
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++ -static")

include_directories(.)

add_executable(challenge_itat
        src/GameScheduler.cpp
        src/GameScheduler.hpp
        src/Resources.hpp
        src/main.cpp
        src/Tournament.cpp
        src/Tournament.hpp
        src/UiHandler.hpp
        src/UiHandler.cpp
        src/Controller.hpp
        src/Controller.cpp
        src/UiResources.hpp
        src/Logic.hpp
        src/Logic.cpp
        src/Teams.hpp
        src/Teams.cpp
)
