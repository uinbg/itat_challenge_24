//
// Created by mackd on 08/08/2024.
//

#pragma once
#include <algorithm>
#include <random>
#include "Resources.hpp"
#include "Teams.hpp"

class Logic
{
public:
    static void setMatchPoints(Match& match);
    static void updateEliminated(Teams& teams, const std::vector<Match>& todayMatches, MatchDay day);
    static bool isGroupStage(MatchDay day);
    static void addMatchResultsToNation(Teams& teams, const std::vector<Match>& todayMatches);
    static MatchResult simulateMatch(const Match& match);
private:
    static void updateWinRatio(Nation& nation, int points);
    static void eliminate(Teams& teams, const std::vector<Match>& todayMatches);
    static double getPrestige(const std::string& nationName);
};

