//
// Created by mackd on 25/07/2024.
//

#pragma once
#include "Resources.hpp"
#include "Tournament.hpp"
#include "UiHandler.hpp"

class Controller
{
public:
    Controller(Tournament& t, UiHandler& v);
    void start();
private:
    bool isRunning = true;
    Tournament tournament;
    UiHandler uiHandler;
};
