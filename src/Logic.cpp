//
// Created by mackd on 08/08/2024.
//

#include "Logic.hpp"

// Sets the match points based on the goals scored by each team
void Logic::setMatchPoints(Match& match)
{
    if(match.goalsA > match.goalsB){
        match.pointsA = 3;
        match.pointsB = 0;
        return;
    }
    if(match.goalsA < match.goalsB){
        match.pointsA = 0;
        match.pointsB = 3;
        return;
    }
    if(match.goalsA == match.goalsB){
        match.pointsA = 1;
        match.pointsB = 1;
        return;
    }
    throw std::invalid_argument("setMatchPoints exception");
}

// Checks if the given match day is part of the group stage
bool Logic::isGroupStage(MatchDay day)
{
    return day == MatchDay::GROUP_STAGE_1 || day == MatchDay::GROUP_STAGE_2 || day == MatchDay::GROUP_STAGE_3;
}

// Updates the elimination status of teams based on the match day
void Logic::updateEliminated(Teams& teams, const std::vector<Match>& todayMatches, MatchDay day)
{
    if(day == MatchDay::ROUND_OF_16)
    {
        int len = static_cast<int>(teams.getLength());
        for(int i = 0; i < 8; i++)
        {
            teams.byRank(len-i).eliminated = true;
        }
    }
    else
    {
        eliminate(teams, todayMatches);
    }
}

// Eliminates the losing team from today's matches
void Logic::eliminate(Teams& teams, const std::vector<Match>& todayMatches)
{
    for(const auto& match: todayMatches)
    {
        if(match.pointsA > match.pointsB)
        {
            teams.byName(match.teamBName).eliminated = true;
        }
        //tie breaker
        else if(match.pointsA == match.pointsB){
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_int_distribution<> dis(0, 1);
            double a = dis(gen);
            if(a > 0.5)
            {
                teams.byName(match.teamBName).eliminated = true;
            }
            else
            {
                teams.byName(match.teamAName).eliminated = true;
            }
        }
        else
        {
            teams.byName(match.teamAName).eliminated = true;
        }
    }
}

// Adds the results of today's matches to the respective nations
void Logic::addMatchResultsToNation(Teams& teams, const std::vector<Match>& todayMatches)
{
    for( auto& match: todayMatches)
    {
        Nation& teamA = teams.byName(match.teamAName);
        teamA.goalDiffPlus += match.goalsA;
        teamA.goalDiffMinus += match.goalsB;
        teamA.points += match.pointsA;
        teamA.gamesPlayed++;
        updateWinRatio(teamA, match.pointsA);

        Nation& teamB = teams.byName(match.teamBName);
        teamB.goalDiffPlus += match.goalsB;
        teamB.goalDiffMinus += match.goalsA;
        teamB.points += match.pointsB;
        teamB.gamesPlayed++;
        updateWinRatio(teamB, match.pointsB);
    }
}

// Updates the win ratio of a nation based on the points earned in a match
void Logic::updateWinRatio(Nation& nation, int points)
{
    switch (points)
    {
        case 0:
            nation.gamesLost++;
            break;
        case 1:
            nation.gamesDrawn++;
            break;
        case 3:
            nation.gamesWon++;
            break;
        default:
            throw std::invalid_argument("updateWinRatio exception");
    }
    nation.winRatio = nation.gamesWon*100.0/nation.gamesPlayed;
}

// Simulates a match and returns the result
MatchResult Logic::simulateMatch(const Match& match)
{
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::normal_distribution<> randomFactor(0.0, 0.5); // Random factor with mean 0 and standard deviation 0.5

    // Retrieve the prestige factors
    const double teamAPrestige = getPrestige(match.teamAName);
    const double teamBPrestige = getPrestige(match.teamBName);

    // Calculate base goal range influenced by prestige factors and random value
    double baseGoalsA = std::log(teamAPrestige) * 2.0 + randomFactor(gen);
    double baseGoalsB = std::log(teamBPrestige) * 2.0 + randomFactor(gen);

    // Adjust base goals by opponent's prestige
    baseGoalsA /= std::log(teamBPrestige);
    baseGoalsB /= std::log(teamAPrestige);

    // Ensure goals are non-negative
    baseGoalsA = std::max(0.0, baseGoalsA);
    baseGoalsB = std::max(0.0, baseGoalsB);

    // Define goal distribution
    std::normal_distribution<> distA(baseGoalsA, 1.0); // Mean is baseGoalsA, standard deviation is 1
    std::normal_distribution<> distB(baseGoalsB, 1.0); // Mean is baseGoalsB, standard deviation is 1

    MatchResult result{};
    result.goalsA = std::max(0, static_cast<int>(distA(gen)));
    result.goalsB = std::max(0, static_cast<int>(distB(gen)));
    return result;
}

// Retrieves the prestige of a nation by its name
double Logic::getPrestige(const std::string& nationName)
{
    for(const NationPrestige& np : consts::nationPrestigeList)
    {
        if(np.nationName == nationName)
        {
            return np.prestige;
        }
    }
    throw std::invalid_argument("getPrestige exception");
}