//
// Created by mackd on 12/08/2024.
//

#pragma once
#include "Resources.hpp"
#include <stdexcept>
#include <algorithm>

class Teams
{
public:
    Teams(const Nation* initList, int size);
    [[nodiscard]] const std::vector<Nation>& getVec() const;
    [[nodiscard]] size_t getLength() const;
    Nation& byName(const std::string& name);
    Nation& byRank(int rank);
    [[nodiscard]] Nation byRank(int rank) const;
    Nation& byId(TeamId id);
    void updateRankings(bool isGroupStage);
private:
    std::vector<char> groupNames;
    std::vector<Nation> teams;
private:
    static bool compareStats(const Nation& a, const Nation& b);
};
