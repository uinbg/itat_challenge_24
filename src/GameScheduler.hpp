//
// Created by mackd on 25/07/2024.
//

#pragma once
#include "Resources.hpp"
#include "Teams.hpp"
class GameScheduler
{
public:
    [[nodiscard]] static std::vector<Match> getGroupSchedule(int day) ;
    [[nodiscard]] static std::vector<Match> getKoSchedule(const Teams& teams) ;
};
