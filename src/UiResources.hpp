//
// Created by mackd on 28/07/2024.
//

#ifndef CHALLENGE_ITAT_UIRESOURCES_HPP
#define CHALLENGE_ITAT_UIRESOURCES_HPP

#include <map>
#include <string>

namespace uiConsts{
    enum UiStrings {
        TITLE_MSG,
        SELECT_TEAM,
        SELECT_TEAM_ERROR,
        ENTER_RESULT,
        UPCOMING_GAME,
        RETURN_MSG,
        END_MSG,
        EXIT_MSG,
        MENU_ERROR,
        USER_TEAM,
        DRAW_ERROR,
        WIN_MSG,
        // Add more as needed above
        SIZE,
    };
    const std::map<UiStrings, std::string> uiStrConsts = {
            {END_MSG, "The tournament has ended. Your Nation was eliminated."},
            {USER_TEAM, "Your Team is: "},
            {MENU_ERROR, "Invalid input. Please select a number from the menu..."},
            {TITLE_MSG, "Euro2024 Tournament Simulator"},
            {UPCOMING_GAME, "Upcoming Game: "},
            {SELECT_TEAM, "Enter the number of the team you want to select:"},
            {SELECT_TEAM_ERROR, "Invalid input. Please select a string in the format 'A1': "},
            {ENTER_RESULT, "Enter Game Results: "},
            {RETURN_MSG, "Enter \"-b\" to return to the main menu..."},
            {EXIT_MSG, "Enter anything to exit..."},
            {DRAW_ERROR, "Draw not allowed during Knockout Stage."},
            {WIN_MSG, "Congratulations! Your Team won the tournament!"}
            // Add more as needed
    };
    const std::map<MatchDay, std::string> matchdayStr = {
            {MatchDay::GROUP_STAGE_1, "Group Stage Day 1"},
            {MatchDay::GROUP_STAGE_2, "Group Stage Day 2"},
            {MatchDay::GROUP_STAGE_3, "Group Stage Day 3"},
            {MatchDay::ROUND_OF_16, "Round of Sixteen"},
            {MatchDay::QUARTER_FINAL, "Quarter Finals"},
            {MatchDay::SEMI_FINAL, "Semi Finals"},
            {MatchDay::FINAL, "Final"}
    };
}


#endif //CHALLENGE_ITAT_UIRESOURCES_HPP
