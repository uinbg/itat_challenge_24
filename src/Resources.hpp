//
// Created by mackd on 25/07/2024.
//

#ifndef CHALLENGE_ITAT_RESOURCES_HPP
#define CHALLENGE_ITAT_RESOURCES_HPP

#include <array>
#include <string>
#include <map>
#include <vector>

enum class UserAction
{
    ENTER_RESULT,
    SHOW_STATS,
    INVALID,
    // Add more as needed above
    SIZE,
};

enum class MatchDay
{
    GROUP_STAGE_1,
    GROUP_STAGE_2,
    GROUP_STAGE_3,
    ROUND_OF_16,
    QUARTER_FINAL,
    SEMI_FINAL,
    FINAL,
    // Add more as needed above
    SIZE,
};

struct TeamId
{
    char group = 'A';
    int number = 0;

    bool operator==(const TeamId& other) const
    {
        return group == other.group && number == other.number;
    }

    bool operator!=(const TeamId& other) const
    {
        return !(*this == other);
    }
};

struct Match
{
    TeamId teamAId{};
    std::string teamAName{};
    TeamId teamBId{};
    std::string teamBName{};
    int pointsA = 0;
    int goalsA = 0;
    int pointsB = 0;
    int goalsB = 0;
    bool played = false;
};

struct DayInfo
{
    MatchDay day = MatchDay::GROUP_STAGE_1;
    Match upcomingMatch{};
    std::string favourite{};
};

struct MatchResult
{
    int goalsA;
    int goalsB;
};

struct Nation
{
    std::string nationName;
    TeamId teamId;
    int groupPlacement;
    int globalPlacement;
    bool eliminated;
    int goalDiffPlus;
    int goalDiffMinus;
    int points;
    int gamesPlayed;
    int gamesWon;
    int gamesDrawn;
    int gamesLost;
    double winRatio;
};

struct NationPrestige
{
    std::string nationName;
    double prestige;
};

namespace consts
{
    static const char groupNames[6] = {'A', 'B', 'C', 'D', 'E', 'F'};
    static constexpr int nationCount = 24;
    static const Nation nationList[nationCount] = {
            {"Germany",        {'A', 1}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Scotland",       {'A', 2}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Hungary",        {'A', 3}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Switzerland",    {'A', 4}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Spain",          {'B', 1}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Croatia",        {'B', 2}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Italy",          {'B', 3}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Albania",        {'B', 4}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Slovenia",       {'C', 1}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Denmark",        {'C', 2}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Serbia",         {'C', 3}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"England",        {'C', 4}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"France",         {'D', 1}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Netherlands",    {'D', 2}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Poland",         {'D', 3}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Austria",        {'D', 4}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Belgium",        {'E', 1}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Slovakia",       {'E', 2}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Ukraine",        {'E', 3}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Romania",        {'E', 4}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Portugal",       {'F', 1}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Turkey",         {'F', 2}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Czech Republic", {'F', 3}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0},
            {"Georgia",        {'F', 4}, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0.0}
    };
    static constexpr int groupDayMatches = 12;
    static const Match groupPlan[3 * 24] = {
            // MatchDay 1
            {{'A', 1}, "Germany",        {'A', 2}, "Scotland"},
            {{'A', 3}, "Hungary",        {'A', 4}, "Switzerland"},
            {{'B', 1}, "Spain",          {'B', 2}, "Croatia"},
            {{'B', 3}, "Italy",          {'B', 4}, "Albania"},
            {{'C', 1}, "Slovenia",       {'C', 2}, "Denmark"},
            {{'C', 3}, "Serbia",         {'C', 4}, "England"},
            {{'D', 1}, "France",         {'D', 2}, "Netherlands"},
            {{'D', 3}, "Poland",         {'D', 4}, "Austria"},
            {{'E', 1}, "Belgium",        {'E', 2}, "Slovakia"},
            {{'E', 3}, "Ukraine",        {'E', 4}, "Romania"},
            {{'F', 1}, "Portugal",       {'F', 2}, "Turkey"},
            {{'F', 3}, "Czech Republic", {'F', 4}, "Georgia"},
            // MatchDay 2
            {{'A', 1}, "Germany",        {'A', 3}, "Hungary"},
            {{'A', 2}, "Scotland",       {'A', 4}, "Switzerland"},
            {{'B', 1}, "Spain",          {'B', 3}, "Italy"},
            {{'B', 2}, "Croatia",        {'B', 4}, "Albania"},
            {{'C', 1}, "Slovenia",       {'C', 3}, "Serbia"},
            {{'C', 2}, "Denmark",        {'C', 4}, "England"},
            {{'D', 1}, "France",         {'D', 3}, "Poland"},
            {{'D', 2}, "Netherlands",    {'D', 4}, "Austria"},
            {{'E', 1}, "Belgium",        {'E', 3}, "Ukraine"},
            {{'E', 2}, "Slovakia",       {'E', 4}, "Romania"},
            {{'F', 1}, "Portugal",       {'F', 3}, "Czech Republic"},
            {{'F', 2}, "Turkey",         {'F', 4}, "Georgia"},
            // MatchDay 3
            {{'A', 1}, "Germany",        {'A', 4}, "Switzerland"},
            {{'A', 2}, "Scotland",       {'A', 3}, "Hungary"},
            {{'B', 1}, "Spain",          {'B', 4}, "Albania"},
            {{'B', 2}, "Croatia",        {'B', 3}, "Italy"},
            {{'C', 1}, "Slovenia",       {'C', 4}, "England"},
            {{'C', 2}, "Denmark",        {'C', 3}, "Serbia"},
            {{'D', 1}, "France",         {'D', 4}, "Austria"},
            {{'D', 2}, "Netherlands",    {'D', 3}, "Poland"},
            {{'E', 1}, "Belgium",        {'E', 4}, "Romania"},
            {{'E', 2}, "Slovakia",       {'E', 3}, "Ukraine"},
            {{'F', 1}, "Portugal",       {'F', 4}, "Georgia"},
            {{'F', 2}, "Turkey",         {'F', 3}, "Czech Republic"}
    };
    const std::array<NationPrestige, nationCount> nationPrestigeList = {{
        {"Germany", 90.0},
        {"Scotland", 50.0},
        {"Hungary", 45.0},
        {"Switzerland", 65.0},
        {"Spain", 90.0},
        {"Croatia", 70.0},
        {"Italy", 85.0},
        {"Albania", 35.0},
        {"Slovenia", 45.0},
        {"Denmark", 65.0},
        {"Serbia", 60.0},
        {"England", 80.0},
        {"France", 85.0},
        {"Netherlands", 80.0},
        {"Poland", 60.0},
        {"Austria", 55.0},
        {"Belgium", 75.0},
        {"Slovakia", 45.0},
        {"Ukraine", 55.0},
        {"Romania", 50.0},
        {"Portugal", 80.0},
        {"Turkey", 60.0},
        {"Czech Republic", 55.0},
        {"Georgia", 40.0}
    }};
}

#endif //CHALLENGE_ITAT_RESOURCES_HPP
