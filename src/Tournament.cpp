//
// Created by mackd on 25/07/2024.
//

#include <iostream>
#include "Tournament.hpp"

// Constructor for the Tournament class
Tournament::Tournament(const Nation* initList, int size)
        :
        userOut(false), // Initialize userOut to false
        groupStage(true), // Initialize groupStage to true
        gameDayNo(0), // Initialize gameDayNo to 0
        favourite({'A',1}), // Initialize favourite team
        todayUserMatch(nullptr), // Initialize todayUserMatch to nullptr
        todayMatches(), // Initialize todayMatches
        teams(Teams(initList, size)) // Initialize teams with the given list and size
{
}

// Get the teams participating in the tournament
const Teams& Tournament::getTeams()
{
    return teams;
}

// Set the favourite team and update today's matches and user match
void Tournament::setFavourite(TeamId teamId)
{
    favourite = teams.byId(teamId).nationName;
    todayMatches = GameScheduler::getGroupSchedule(gameDayNo);
    todayUserMatch = updateUserMatch();
}

// Get the user's match for today
Match Tournament::getUserMatch() const
{
    return *todayUserMatch;
}

// Update the user's match for today
Match* Tournament::updateUserMatch()
{
    for (auto& match: todayMatches)
    {
        if (match.teamAName == favourite || match.teamBName == favourite)
        {
            return &match;
        }
    }
    std::cout << favourite << std::endl;
    throw std::invalid_argument("updateUserMatch exception");
}

// Save the user's match result
void Tournament::saveUserResult(MatchResult result)
{
    todayUserMatch->goalsA = result.goalsA;
    todayUserMatch->goalsB = result.goalsB;
    Logic::setMatchPoints(*todayUserMatch);
    todayUserMatch->played = true;
}

// Get information about the current day
DayInfo Tournament::getDayInfo() const
{
    DayInfo info;
    info.day = static_cast<MatchDay>(gameDayNo);
    info.favourite = favourite;
    info.upcomingMatch = *todayUserMatch;
    return info;
}

// Simulate the matches for the current game day
void Tournament::simulateGameDay()
{
    for (auto& match: todayMatches)
    {
        if (!match.played)
        {
            MatchResult simulated = Logic::simulateMatch(match);
            match.goalsA = simulated.goalsA;
            match.goalsB = simulated.goalsB;
            Logic::setMatchPoints(match);
            match.played = true;
        }
    }
    Logic::addMatchResultsToNation(teams, todayMatches);
    teams.updateRankings(groupStage);
}

// End the current game day and prepare for the next one
void Tournament::endDay()
{
    gameDayNo++;
    groupStage = Logic::isGroupStage(static_cast<MatchDay>(gameDayNo));
    if (!groupStage)
    {
        Logic::updateEliminated(teams, todayMatches, static_cast<MatchDay>(gameDayNo));
        todayMatches = GameScheduler::getKoSchedule(teams);
    }
    else
    {
        todayMatches = GameScheduler::getGroupSchedule(gameDayNo);
    }

    userOut = getIsOver();
    if(!userOut && static_cast<MatchDay>(gameDayNo) != MatchDay::SIZE){
        todayUserMatch = updateUserMatch();
    }
}

bool Tournament::getIsOver()
{
    if(teams.byName(favourite).eliminated){
        return true;
    }
    return false;
}

bool Tournament::isOver() const
{
    return userOut;
}

bool Tournament::isWon() const
{
    return !userOut && gameDayNo == static_cast<int>(MatchDay::SIZE);
}

bool Tournament::isGroupStage() const
{
    return groupStage;
}

void Tournament::simulateRest()
{
    while(true){
        simulateGameDay();
        endDay();
        if(gameDayNo == static_cast<int>(MatchDay::SIZE)){
            break;
        }
    }
}


