//
// Created by mackd on 25/07/2024.
//

#pragma once
#include <iostream>
#include <iomanip>
#include <limits>
#include "Resources.hpp"
#include "UiResources.hpp"
#include "Tournament.hpp"

#ifdef WIN32
#include <windows.h>
#endif

using namespace uiConsts;

class UiHandler
{
public:
    UserAction showMenu(const DayInfo& info);
    static TeamId selectNation(const Teams& teams);
    static MatchResult enterResult(const Match& match, bool isGroupStage);
    static void showStats(const Teams& teams);
    static void showEndScreen(const Teams& teams);
    static void showWinScreen(const Teams& teams);

private:
    static void printSpace(const std::string& str, int width);
    static void printStats(const Teams& teams);
    static bool isValidId(const std::string& userInput);
    static bool isValidMenuChoice(const std::string& userInput);
    static void getMatchResult(const Match& match, MatchResult& result);
    static void clear();
private:
    const std::map<UserAction, std::string> menuStrConsts = {
            {UserAction::ENTER_RESULT, "Enter Game Result: "},
            {UserAction::SHOW_STATS, "Show Statistics: "},
            // Add more as needed
    };
};


