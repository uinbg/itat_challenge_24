//
// Created by mackd on 25/07/2024.
//

#include "Controller.hpp"

// Constructor for the Controller class
Controller::Controller(Tournament& t, UiHandler& v)
        :
        tournament(std::move(t)),
        uiHandler(v)
{
}

// Starts the tournament control loop
void Controller::start()
{
    // Set the favorite team
    tournament.setFavourite(UiHandler::selectNation(tournament.getTeams()));

    // Main loop for the tournament
    while(isRunning){
        // Show the menu and get user action
        UserAction choice = uiHandler.showMenu(tournament.getDayInfo());

        switch (choice)
        {
            case UserAction::SHOW_STATS:
                // Show statistics of the teams
                UiHandler::showStats(tournament.getTeams());
                break;

            case UserAction::ENTER_RESULT:
                // Save user-entered result
                tournament.saveUserResult(
                        UiHandler::enterResult(tournament.getUserMatch(), tournament.isGroupStage())
                );
                // Simulate the game day
                tournament.simulateGameDay();
                // End the current day
                tournament.endDay();

                // Check if the tournament is over
                if(tournament.isOver()){
                    // Simulate the rest of the tournament
                    tournament.simulateRest();
                    // Show the end screen
                    UiHandler::showEndScreen(tournament.getTeams());
                    isRunning = false;
                }

                // Check if the tournament is won
                if(tournament.isWon())
                {
                    // Show the win screen
                    UiHandler::showWinScreen(tournament.getTeams());
                    isRunning = false;
                }
                break;

            default:
                // Exit the loop
                isRunning = false;
                break;
        }
    }
}