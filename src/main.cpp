//
// Created by mackd on 25/07/2024.
//

#include "src/Controller.hpp"
#include "src/UiHandler.hpp"
#include "src/Tournament.hpp"

int main(){
    UiHandler uiHandler;
    Tournament tournament = Tournament(consts::nationList, consts:: nationCount);
    Controller controller(tournament, uiHandler);

    controller.start();
    return 0;
}
