#include "UiHandler.hpp"

// Clear the console screen
void UiHandler::clear()
{
#ifdef WIN32
    // Windows
    std::system("CLS");
#else
    // Linux / Unix
    std::system("clear");
#endif
}

// Select a nation from the list of teams
TeamId UiHandler::selectNation(const Teams& teams)
{
    clear();
    std::cout << uiStrConsts.at(UiStrings::SELECT_TEAM) << std::endl;
    auto teamsVec = teams.getVec();
    for(int i = 0; i < teamsVec.size(); i += 2)
    {
        std::cout << teamsVec[i].teamId.group << teamsVec[i].teamId.number <<  ": " << teamsVec[i].nationName;
        printSpace(teamsVec[i].nationName, 20);
        if(i + 1 < teamsVec.size()){
            std::cout << teamsVec[i+1].teamId.group << teamsVec[i+1].teamId.number << ": " << teamsVec[i + 1].nationName << std::endl;
        }
        else{
            std::cout << std::endl;
        }
    }
    std::string choice;
    std::cin >> choice;
    while (!isValidId(choice))
    {
        std::cout << uiStrConsts.at(UiStrings::SELECT_TEAM_ERROR) << std::endl;
        std::cin >> choice;
    }
    char letter = choice[0];
    int number = choice[1] - '0'; // char to int: subtract '0'
    return TeamId({letter, number});
}

// Show the main menu and get the user's action
UserAction UiHandler::showMenu(const DayInfo& info)
{
    clear();
    std::cout << uiStrConsts.at(UiStrings::TITLE_MSG) << " - " << matchdayStr.at(info.day) << std::endl;
    std::cout << uiStrConsts.at(UiStrings::USER_TEAM) << info.favourite << std::endl;
    std::cout << uiStrConsts.at(UiStrings::UPCOMING_GAME) << info.upcomingMatch.teamAName << " vs " << info.upcomingMatch.teamBName << std::endl;
    for(auto& [key, value] : menuStrConsts)
    {
        std::cout << static_cast<int>(key) << ". " << value << std::endl;
    }
    std::string choice;
    std::cin >> choice;
    while (!isValidMenuChoice(choice))
    {
        std::cout << uiStrConsts.at(UiStrings::MENU_ERROR) << std::endl;
        std::cin >> choice;
    }
    int result = choice[0] - '0';
    return static_cast<UserAction>(result);
}

// Enter the result of a match
MatchResult UiHandler::enterResult(const Match& match, bool isGroupStage)
{
    clear();
    std::cout << uiStrConsts.at(UiStrings::TITLE_MSG) << std::endl;
    std::cout << uiStrConsts.at(UiStrings::ENTER_RESULT) << std::endl;
    MatchResult matchResult{};
    getMatchResult(match, matchResult);
    while(!isGroupStage && matchResult.goalsA == matchResult.goalsB)
    {
        std::cout << uiStrConsts.at(UiStrings::DRAW_ERROR) << std::endl;
        getMatchResult(match, matchResult);
    }

    return matchResult;
}

// Show the statistics of the teams
void UiHandler::showStats(const Teams& teams) {
    clear();
    std::cout << uiStrConsts.at(UiStrings::RETURN_MSG) << std::endl;
    printStats(teams);
    std::string command;
    std::cin >> command;
    while (command != "-b"){
        std::cout << uiStrConsts.at(UiStrings::RETURN_MSG) << std::endl;
        std::cin >> command;
    }
}

// Show the end screen with the final results
void UiHandler::showEndScreen(const Teams& teams)
{
    clear();
    std::cout << uiStrConsts.at(UiStrings::END_MSG) << std::endl;
    for(const Nation& nation : teams.getVec())
    {
        if(!nation.eliminated)
        {
            std::cout << "The winner of the tournament was: " << nation.nationName << std::endl;

        }
    }
    std::cout << "The final results are: " << std::endl;
    printStats(teams);
    std::cout << uiStrConsts.at(UiStrings::EXIT_MSG) << std::endl;
    std::cin >> std::ws;
}

// Show the win screen if the user's nation wins the tournament
void UiHandler::showWinScreen(const Teams& teams)
{
    clear();
    std::cout << uiStrConsts.at(UiStrings::WIN_MSG) << std::endl;
    std::cout << "The final results are: " << std::endl;
    printStats(teams);
    std::cout << uiStrConsts.at(UiStrings::EXIT_MSG) << std::endl;
    std::cin >> std::ws;
}

// Validate if the user input is a valid team ID
bool UiHandler::isValidId(const std::string& userInput)
{
    if(userInput.length() != 2 || !std::isalpha(userInput[0]) || !std::isdigit(userInput[1]))
    {
        return false;
    }
    if(userInput[0] < 'A' || userInput[0] > 'F')
    {
        return false;
    }
    if(userInput[1] < '1' || userInput[1] > '4')
    {
        return false;
    }
    return true;
}

// Print the statistics of the teams
void UiHandler::printStats(const Teams& teams)
{
    for(const Nation& nation : teams.getVec())
    {
        std::cout
                << nation.teamId.group << nation.teamId.number << ": " << nation.nationName;
        printSpace(nation.nationName, 15);
        printSpace(std::to_string(nation.teamId.number), 3);
        std::cout
                << " | Points: " << nation.points
                << " | Group-Rank: " << nation.groupPlacement
                << " | Rank: " << nation.globalPlacement;
        printSpace(std::to_string(nation.globalPlacement), 3);
        std::cout
                << " | Wins: " << nation.gamesWon
                << " | Elim.: " << (nation.eliminated ? "Yes" : "No ")
                << " | WR: " << std::fixed << std::setprecision(1) << nation.winRatio << "%"
                << " | GD: " << nation.goalDiffPlus << ":-" << nation.goalDiffMinus
                << std::endl;
    }
}

// Print spaces to align text
void UiHandler::printSpace(const std::string& str, int width)
{
    if(str.length() >= width) throw std::invalid_argument("printSpace exception");
    for(int i = 0; i < width - str.length(); i++)
    {
        std::cout << " ";
    }
}

// Validate if the user input is a valid menu choice
bool UiHandler::isValidMenuChoice(const std::string& userInput)
{
    if(userInput.length() != 1 || !std::isdigit(userInput[0]))
    {
        return false;
    }
    if(userInput[0] < '0' || userInput[0] > '1')
    {
        return false;
    }
    return true;
}

// Get the match result from the user
void UiHandler::getMatchResult(const Match& match, MatchResult& matchResult)
{
    for(int i = 0; i < 2; i++)
    {
        std::string teamName;
        if (i == 0) { teamName = match.teamAName; }
        else { teamName = match.teamBName; }
        std::cout << "Enter goals for " << teamName << ": ";
        int goalAmount;
        std::cin >> goalAmount;
        while(std::cin.fail() || goalAmount < 0 || goalAmount > 99)
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Invalid Input. Enter positive Integer" << std::endl;
            std::cin >> goalAmount;
        }
        if (i == 0) { matchResult.goalsA = goalAmount; }
        else { matchResult.goalsB = goalAmount;}
    }
}