//
// Created by mackd on 12/08/2024.
//

#include "Teams.hpp"

// Constructor for Teams class
Teams::Teams(const Nation* initList, int size)
{
    // Reserve space for the teams vector
    teams.reserve(size);
    // Initialize teams vector with the given list
    for (int i = 0; i < size; i++)
    {
        teams.push_back(initList[i]);
    }
    // Initialize groupNames vector with predefined group names
    for(char c : consts::groupNames)
    {
        groupNames.push_back(c);
    }
}

// Get the vector of teams
const std::vector<Nation>& Teams::getVec() const
{
    return teams;
}

// Get the number of teams
size_t Teams::getLength() const
{
    return teams.size();
}

// Get a team by its global rank
Nation& Teams::byRank(int rank)
{
    for(Nation& team: teams)
    {
        if(team.globalPlacement == rank)
        {
            return team;
        }
    }
    throw std::invalid_argument("byRank exception");
}

// Get a team by its global rank (const version)
Nation Teams::byRank(int rank) const
{
    for(Nation team: teams)
    {
        if(team.globalPlacement == rank)
        {
            return team;
        }
    }
    throw std::invalid_argument("byRank exception");
}

// Get a team by its ID
Nation& Teams::byId(TeamId id)
{
    for (Nation& team: teams)
    {
        if (team.teamId == id)
        {
            return team;
        }
    }
    throw std::invalid_argument("byId exception");
}

// Get a team by its name
Nation& Teams::byName(const std::string& name)
{
    for(Nation& team: teams)
    {
        if(team.nationName == name)
        {
            return team;
        }
    }
    throw std::invalid_argument("byName exception");
}

// Update the rankings of the teams
void Teams::updateRankings(bool isGroupStage)
{
    // Sort teams based on their stats
    std::sort(teams.begin(), teams.end(), compareStats);
    // Update global placement for each team
    for (int i = 0; i < teams.size(); i++)
    {
        teams[i].globalPlacement = i + 1;
    }

    if(isGroupStage)
    {
        // Update group placement for each team in the group stage
        for (char groupName: groupNames)
        {
            std::vector<Nation*> temp;
            for (Nation& team: teams)
            {
                if (team.teamId.group == groupName)
                {
                    temp.push_back(&team);
                }
            }
            std::sort(temp.begin(), temp.end(), [](Nation* a, Nation* b)
            {
                return a->globalPlacement < b->globalPlacement;
            });

            for (int i = 0; i < temp.size(); i++)
            {
                temp[i]->groupPlacement = i + 1;
            }
        }
    }
    else
    {
        // Update team IDs for knockout stage
        for (Nation& team: teams)
        {
            team.teamId.group = 'W';
            team.teamId.number = team.globalPlacement;
        }
    }
}

// Compare stats of two teams for sorting
bool Teams::compareStats(const Nation& a, const Nation& b)
{
    // Sort by points
    if (a.points > b.points)
    {
        return true;
    }
    if(a.points < b.points)
    {
        return false;
    }
    // Sort by goal difference
    if (a.goalDiffPlus - a.goalDiffMinus > b.goalDiffPlus - b.goalDiffMinus)
    {
        return true;
    }
    if (a.goalDiffPlus - a.goalDiffMinus < b.goalDiffPlus - b.goalDiffMinus)
    {
        return false;
    }
    // Sort by goals scored
    if (a.goalDiffPlus > b.goalDiffPlus)
    {
        return true;
    }
    return false;
}