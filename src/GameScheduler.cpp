//
// Created by mackd on 25/07/2024.
//
#include "GameScheduler.hpp"

// Generates the group stage schedule for a given day
std::vector<Match> GameScheduler::getGroupSchedule(int day)
{
    std::vector<Match> result;
    int matchNo = consts::groupDayMatches;
    // Retrieve the matches scheduled for the given day
    for(int i = 0; i < matchNo; i++)
    {
        Match match = Match();
        match.teamAId = consts::groupPlan[day*matchNo+i].teamAId;
        match.teamAName = consts::groupPlan[day*matchNo+i].teamAName;
        match.teamBId = consts::groupPlan[day*matchNo+i].teamBId;
        match.teamBName = consts::groupPlan[day*matchNo+i].teamBName;
        result.push_back(match);
    }
    return result;
}

// Generates the knockout stage schedule based on the active teams
std::vector<Match> GameScheduler::getKoSchedule(const Teams& teams)
{
    std::vector<Nation> activeNations;
    // Collect all active (non-eliminated) nations
    for(const auto& nation: teams.getVec())
    {
        if(!nation.eliminated)
        {
            activeNations.push_back(nation);
        }
    }
    std::vector<Match> result;
    size_t len = activeNations.size();
    // Pair the nations for knockout matches
    for(int i = 0; i < len; i++)
    {
        if(i >= len-i-1){
            break;
        }
        Match match = Match();
        match.teamAId = activeNations[i].teamId;
        match.teamAName = activeNations[i].nationName;
        match.teamBId = activeNations[len-i-1].teamId;
        match.teamBName = activeNations[len-i-1].nationName;
        result.push_back(match);
    }
    return result;
}