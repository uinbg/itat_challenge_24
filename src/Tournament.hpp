//
// Created by mackd on 25/07/2024.
//

#pragma once
#include "Resources.hpp"
#include "GameScheduler.hpp"
#include "Logic.hpp"
#include "Teams.hpp"


class Tournament
{
public:
    explicit Tournament(const Nation* initList, int size);
    void setFavourite(TeamId teamId); //set favourite team
    [[nodiscard]] Match getUserMatch() const; //getVec next Match
    [[nodiscard]] const Teams& getTeams(); //getVec list of teams
    [[nodiscard]] DayInfo getDayInfo() const; //getVec info about the current day
    [[nodiscard]] bool isOver() const;
    [[nodiscard]] bool isWon() const;
    [[nodiscard]] bool isGroupStage() const;
    void simulateGameDay(); //simulate a game day
    void saveUserResult(MatchResult result);
    void endDay();
    void simulateRest();
private:
    bool getIsOver();
    Match* updateUserMatch();
private:
    bool userOut;
    bool groupStage;
    int gameDayNo;
    std::string favourite;
    Match* todayUserMatch{};
    std::vector<Match> todayMatches;
    Teams teams;
};