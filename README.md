Dieses Repository ist eine Kopie von: https://gitlab.kit.edu/uvthh/challenge-itat
Wir wussten nicht, ob alle Gruppenmitglieder ein Repository abgeben müssen. Commit-Histories sind
auf https://gitlab.kit.edu/uvthh/challenge-itat einsehbar (siehe E-Mail).
***
# Euro-2024 Konsolen Simulator 

## Gruppenmitglieder

1. Linus Hofmann (uvthh)
2. David Mack (uinbg)
3. Christoph Lubert (uxyls)

***
## Projektbeschreibung

Das Programm bildet den Spielplan der Europameisterschaft 2024 ab.
Es beinhaltet alle angetretenen Nationalmannschaften. 
Man wählt sein Team zur Simulation aus und trägt die Ergebnisse der Gruppenspiele für diese Mannschaft ein.
Die restlichen Spielergebnisse werden dabei durch Zufall unter Berücksichtigung eines Prestige- und Formfaktors simuliert.
In der K.O.-Phase wird diese Schema fortgesetzt. 
Nach jedem Spieltag können die aktuellen Statistiken eingesehen werden.

***
## Bedienungsanleitung

1. **Programm starten**: Das Programm ist mit g++ kompilierbar und wird als Konsolenapplikation ausgeführt.
2. **Team auswählen**: Wählen Sie Ihr Favouritenteam aus der Liste der Nationalmannschaften durch Eingabe des Kürzels z.B. "A1" aus.
3. **Menü**: Nach der Auswahl des Teams wird das Menü angezeigt. Mit "0" können Sie Ergebnisse Eintragen, mit "1" die aktuellen Statistiken einsehen. 
4. **Statistiken Ansehen** alle Kennzahlen zu den Teams werden aufgelistet. Durch die Eingabe "-b" gelangen Sie zurück ins Menü.
5. **Ergebnisse eingeben**: Tragen Sie die Ergebnisse der Gruppenspiele für Ihr Team ein (Bestätigen der Eingabe mit Enter). Die Ergebnisse der anderen Spiele werden automatisch simuliert.
6. **Spieltag beenden**: Nach Eingabe der Ergebnisse wird der Spieltag beendet und die aktuellen Statistiken können eingesehen werden.
7. **Spiel beenden**: Das Spiel endet, wenn Ihr Team entweder die Meisterschaft gewinnt oder ausscheidet. Sie können die Endstatistiken und den simulierten Gewinner des Turniers einsehen.

***

## Struktur des Programms
Es wurde versucht, das Programm nach dem MVC-Prinzip zu strukturieren.
Die Klassen `Controller`, `Tournament` und `UiHandler` sind die Hauptkomponenten des Programms.
Die Klasse `Controller` steuert den Ablauf des Programms und verknüpft die anderen Klassen.
Die Klasse `Tournament` repräsentiert das Turnier und enthält alle Informationen zu den Teams und Spielen.
Die Klasse `UiHandler` kümmert sich um die Ein- und Ausgabe und interagiert mit dem Benutzer.
Nachfolgend ist ein Klassendiagramm dargestellt, das die Beziehungen zwischen diesen Klassen zeigt.

```mermaid
classDiagram
    class Controller {
        -bool isRunning
        -Tournament tournament
        -UiHandler uiHandler
        +Controller(Tournament& t, UiHandler& v)
        +void start()
    }

    class Tournament {
        -bool userOut
        -bool groupStage
        -int gameDayNo
        -std::string favourite
        -Match* todayUserMatch
        -std::vector<Match> todayMatches
        -Teams teams
        +Tournament(const Nation* initList, int size)
        +void setFavourite(TeamId teamId)
        +Match getUserMatch() const
        +const Teams& getTeams() const
        +DayInfo getDayInfo() const
        +bool isOver() const
        +bool isWon() const
        +bool isGroupStage() const
        +void simulateGameDay()
        +void saveUserResult(MatchResult result)
        +void endDay()
        +void simulateRest()
    }

    class UiHandler {
        +UserAction showMenu(const DayInfo& info)
        +static TeamId selectNation(const Teams& teams)
        +static MatchResult enterResult(const Match& match, bool isGroupStage)
        +static void showStats(const Teams& teams)
        +static void showEndScreen(const Teams& teams)
        +static void showWinScreen(const Teams& teams)
    }

    Controller --> Tournament : uses
    Controller --> UiHandler : uses

```
***
## Sonstiges
Uns sind ein paar Optimierungsmöglichkeiten aufgefallen, die wir gerne umgesetzt hätten,
jedoch aufgrund der begrenzten Zeit nicht mehr realisieren konnten:
- Stärkere Nutzung von Pointern bei Zugriffen auf spezifische Nation, um for-Schleifen zu reduzieren
- Änderung der Structs in Resources.hpp zu Klassen, damit Datenspeicherungen durch Verkapselung sicherer sind
- Erweiterte Statistiken für einzelne Teams
- Realistischere Simulation der Spiele
- etc.